﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncExplained
{
    class Program
    {
        const int N = 1000000;

        static Task<int> GetPrimesCountAsync(int start, int count)
        {
            Console.WriteLine($"Start counting primes({start}, {count})");
            return Task.Run(()
                => ParallelEnumerable
                        .Range(start, count)
                        .Count(n => Enumerable.Range(2, (int)Math.Sqrt(n) - 1)
                            .All(i => n % i > 0)));
        }

        static async Task DisplayPrimesCount()
        {
            for(int i = 0; i < 10; ++i)
                Console.WriteLine(await GetPrimesCountAsync(i * N + 2, N - 2) + " primes between " + (i * N + 2) + " and " + (i * N));
            Console.WriteLine("Done");
        }

        async static Task<int> LongCalculation()
        {
            Console.WriteLine("Calculation started");
            await Task.Delay(10000);
            Console.WriteLine("Calculation done");
            return 13;
        }

        async static Task Go()
        {
            Console.WriteLine("Go started");
            int result = await LongCalculation();
            Console.WriteLine("*******************");
            Console.WriteLine("Result: " + result);
            Console.WriteLine("Go done");
        }

        // optymalizacja
        class PageLoader
        {
            Dictionary<string, string> _cache = new Dictionary<string, string>();

            public async Task<string> LoadPage(string url)
            {
                if (_cache.ContainsKey(url))
                    return _cache[url];

                return _cache[url] = await new WebClient().DownloadStringTaskAsync(url);
            }
        }

        public async static Task LoadPages()
        {
            string url = "http://www.infotraining.pl";

            PageLoader loader = new PageLoader();

            var timer = Stopwatch.StartNew();

            string result = await loader.LoadPage(url);

            timer.Stop();

            Console.WriteLine(timer.ElapsedMilliseconds);

            Console.WriteLine("\n\n");

            timer.Restart();

            result = await loader.LoadPage(url);

            timer.Stop();

            Console.WriteLine(timer.ElapsedMilliseconds);
        }

        static async Task<int> Fractional(int i)
        {
            Console.WriteLine("ThreadId: " + Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine($"Start F({i})");
            Thread.Sleep(200);
            //await Task.Delay(200);
            if (i == 1)
                return 1;
             return i * await Fractional(i - 1);
        }

        static async Task Caller()
        {
            Console.WriteLine("ThreadId: " + Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine("Start");
            int result = await Fractional(4);
            Console.WriteLine("Result: " + result);
        }


        static void Main(string[] args)
        {
            Task task = null;

            //task = Go();

            //Console.WriteLine("#########");

            //task.Wait();

            //task = DisplayPrimesCount();

            //while (!task.IsCompleted)
            //{
            //    Console.WriteLine("Main work...");
            //    Thread.Sleep(200);
            //}

            //Console.ReadKey(true);

            //LoadPages().Wait();

            task = Caller();

            while (!task.IsCompleted)
            {
                Console.WriteLine("Main work...");
                Thread.Sleep(200);
            }

            //Console.ReadKey(true);
        }
    }
}
