﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncMethod
{
    class Program
    {
        static Task<string> DownloadContentAsync(string url)
        {
            return Task.Run<string>(() => new WebClient().DownloadString(url));
        }

        static async Task SaveAllDataAsync(params string[] urls)
        {
            Task<string>[] tasks = new Task<string>[urls.Length];

            for (int i = 0; i < tasks.Length; ++i)
            {
                Console.WriteLine("Starting downloading: " + urls[i]);
                tasks[i] = DownloadContentAsync(urls[i]);
            }

            Console.WriteLine("Download has started");

            string[] content = await Task.WhenAll(tasks);
            
            Console.WriteLine("Saving a content: ");
            for(int i = 0; i < content.Length; ++i)
            {
                string fileName = new Uri(urls[i]).Host + ".txt";

                using (StreamWriter writer = new StreamWriter(fileName))
                {
                    Console.WriteLine("Saving: " + fileName);
                    await writer.WriteAsync(content[i]);
                    Console.WriteLine("Saved: " + fileName);
                }
            }

            Console.WriteLine("End"); 
        }

        static async Task SaveAnyDataAsync(params string[] urls)
        {
            Task<string>[] tasks = new Task<string>[urls.Length];

            for (int i = 0; i < tasks.Length; ++i)
            {
                Console.WriteLine("Starting downloading: " + urls[i]);
                tasks[i] = DownloadContentAsync(urls[i]);
            }

            Console.WriteLine("Download has started");

            Task<string> contentTask = await Task.WhenAny(tasks);
            string content = await contentTask;

            Console.WriteLine("Saving a content: ");

            string fileName = "data.txt";

            using (StreamWriter writer = new StreamWriter(fileName))
            {
                Console.WriteLine("Saving: " + fileName);
                await writer.WriteAsync(content);
                Console.WriteLine("Saved: " + fileName);
            }
            
            Console.WriteLine("End");
        }

        static async Task MainAsync()
        {
            string[] urls = { "http://www.infotraining.pl", "http://www.google.com", "http://www.gazeta.pl" };

            try
            {
                await SaveAllDataAsync(urls);
            }
            catch (Exception e)
            {
                Console.WriteLine("Caught an exception: " + e.Message);
            }
        }

        static void Main(string[] args)
        {
            MainAsync().Wait();
        }
    }
}
