﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BingImages_Ex
{
    public class BingImagesService
    {
        private const string _catalogUri =
            "http://www.bing.com/hpimagearchive.aspx?format=xml&idx=0&n=8&mbl=1&mkt=en-ww";

        private const string _imageUri = "http://bing.com{0}_1920x1080.jpg";

        private Image GetThumbnail(Stream imageStream)
        {
            using (imageStream)
            {
                var fullBitmap = Image.FromStream(imageStream);
                return new Bitmap(fullBitmap, 192, 108);
            }
        }

        private Task<byte[]> DownloadDataAltAsync(string uri)
        {
            return Task<byte[]>.Run(() => (new WebClient()).DownloadData(uri));
        }

        private async Task<Stream> DownloadDataAsync(string uri)
        {
            HttpClient httpClient = new HttpClient();

            var data = await new WebClient().DownloadDataTaskAsync(uri)
                                    .ConfigureAwait(false);

            var stream = new MemoryStream(data, false);

            return stream;
        }

        public async Task<WallpapersInfo> LoadImagesAsync()
        {
            var sw = new Stopwatch();
            sw.Start();

            var client = new WebClient();
            var catalogXml = await client.DownloadStringTaskAsync(_catalogUri).ConfigureAwait(false);

            var xDoc = XDocument.Parse(catalogXml);

            var wallpapersTasks = xDoc
                .Root
                .Elements("image")
                .Select(e =>
                    new
                    {
                        Desc = e.Element("copyright").Value,
                        Url = e.Element("urlBase").Value
                    })
                .Select(async item =>
                    new
                    {
                        item.Desc,
                        FullImageData = await new WebClient().DownloadDataTaskAsync(
                            string.Format(_imageUri, item.Url)).ConfigureAwait(false)
                    });

            var wallpapersItems = await Task.WhenAll(wallpapersTasks).ConfigureAwait(false);

            var wallpapers = wallpapersItems.Select(item =>
                    new WallpaperInfo(
                        GetThumbnail(new MemoryStream(item.FullImageData, false)),
                        item.Desc))
                .ToArray();

            sw.Stop();

            return new WallpapersInfo(sw.ElapsedMilliseconds, wallpapers);
        }
    }
}

