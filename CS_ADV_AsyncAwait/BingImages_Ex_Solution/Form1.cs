﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BingImages_Ex
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async Task btnLoadSync_ClickHelper()
        {
            try
            {
                var service = new BingImagesService();
                var info = await service.LoadImagesAsync();
                RefreshContent(info);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Exception");                
            }            
        }

        private async void btnLoadSync_Click(object sender, EventArgs e)
        {
            var service = new BingImagesService();
            await btnLoadSync_ClickHelper();            
        }

        private void RefreshContent(WallpapersInfo info)
        {
            pnlImages.Controls.Clear();
            pnlImages.Controls.AddRange(
            info.Wallpapers.SelectMany((wallpaper, i) => new Control[]
            {
            new PictureBox
            {
              Left = 4,
              Image = wallpaper.Thumbnail,
              AutoSize = true,
              Top = GetItemTop(wallpaper.Thumbnail.Height, i)
            },
            new Label
            {
              Left = wallpaper.Thumbnail.Width + 8,
              Top = GetItemTop(wallpaper.Thumbnail.Height, i),
              Text = wallpaper.Description,
              AutoSize = true
            }
            }).ToArray());

            lblTime.Text = string.Format("Time: {0}ms", info.Milliseconds);
        }

        private int GetItemTop(int height, int index)
        {
            return index * (height + 8) + 8;
        }
    }
}
