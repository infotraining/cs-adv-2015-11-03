﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BingImages_Ex
{
    public class WallpaperInfo
    {
        private readonly Image _thumbnail;
        private readonly string _description;

        public WallpaperInfo(Image thumbnail, string description)
        {
            _thumbnail = thumbnail;
            _description = description;
        }

        public Image Thumbnail
        {
            get { return _thumbnail; }
        }

        public string Description
        {
            get { return _description; }
        }
    }
}
