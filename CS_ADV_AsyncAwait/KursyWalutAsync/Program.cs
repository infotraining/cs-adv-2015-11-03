﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KursyWalutAsync
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    WebClient client = new WebClient();
        //    string kursyTabelaA = client.DownloadString(@"http://rss.nbp.pl/kursy/TabelaA.xml");

        //    var xmlKursy = XElement.Parse(kursyTabelaA);

        //    var tabele = xmlKursy.Descendants("item").Select(item => new { Title = item.Element("title").Value, Url = item.Element("enclosure").Attribute("url").Value });

        //    foreach (var tabela in tabele)
        //    {
        //        Console.WriteLine(tabela.Title + " - url: " + tabela.Url);

        //        string[] koszykWalut = { "USD", "GBP", "EUR", "CAD" };

        //        string daneNBP = client.DownloadString(tabela.Url);

        //        var xmlDane = XElement.Parse(daneNBP);

        //        var dane = from pozycja in xmlDane.Descendants("pozycja")
        //                   where koszykWalut.Contains(pozycja.Element("kod_waluty").Value)
        //                   select new
        //                   {
        //                       KodWaluty = pozycja.Element("kod_waluty").Value,
        //                       Nazwa = pozycja.Element("nazwa_waluty").Value,
        //                       Przelicznik = pozycja.Element("przelicznik").Value,
        //                       KursSredni = pozycja.Element("kurs_sredni").Value
        //                   };

        //        foreach (var d in dane)
        //            Console.WriteLine("{0,-20}; {1,3} {2,3} ; {3:f4} PLN",
        //                d.Nazwa, d.Przelicznik, d.KodWaluty, d.KursSredni);
        //    }
        //}

        static void Main(string[] args)
        {
            MainAsync().Wait();
        }


        static async Task MainAsync()
        {
            try
            {
                await ProcessAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }                      
        }

        private static async Task ProcessAsync()
        {
            WebClient client = new WebClient();

            string kursyTabelaA = client.DownloadString(@"http://rss.nbp.pl/kursy/TabelaA.xml");

            var xmlKursy = XElement.Parse(kursyTabelaA);

            var tabele = xmlKursy.Descendants("item").Select(item => new { Title = item.Element("title").Value, Url = item.Element("enclosure").Attribute("url").Value });

            List<Task<string>> tasks = new List<Task<string>>();

            foreach (var tabela in tabele)
            {
                Console.WriteLine(tabela.Title + " - url: " + tabela.Url);

                tasks.Add(Task.Run(() => new WebClient().DownloadString(tabela.Url)));               
            }

            string[] daneNBP = await Task.WhenAll(tasks);

            foreach (var tabelaXml in daneNBP)
            {
                var xmlDane = XElement.Parse(tabelaXml);

                string[] koszykWalut = { "USD", "GBP", "EUR", "CAD" };

                var dane = from pozycja in xmlDane.Descendants("pozycja")
                           where koszykWalut.Contains(pozycja.Element("kod_waluty").Value)
                           select new
                           {
                               KodWaluty = pozycja.Element("kod_waluty").Value,
                               Nazwa = pozycja.Element("nazwa_waluty").Value,
                               Przelicznik = pozycja.Element("przelicznik").Value,
                               KursSredni = pozycja.Element("kurs_sredni").Value
                           };

                foreach (var d in dane)
                    Console.WriteLine("{0,-20}; {1,3} {2,3} ; {3:f4} PLN",
                        d.Nazwa, d.Przelicznik, d.KodWaluty, d.KursSredni);

                Console.WriteLine("--------");
            }

            Console.WriteLine("End of processing");
        }
    }
}
