﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Exercise_LinqToDataSet
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void productsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.productsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.northwindDataSet);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'northwindDataSet.Products' table. You can move, or remove it, as needed.
            this.productsTableAdapter.Fill(this.northwindDataSet.Products);
        }

        private void btnSaveXml_Click(object sender, EventArgs e)
        {
            /* TODO: zapisz do pliku xml dane w poniższym formacie
             * <?xml version="1.0" encoding="utf-8"?>
             *     <NorthwindProducts>
             *         <product id="1">
             *           <productName>Herbata Assam</productName>
             *           <unitPrice>12.0000</unitPrice>
             *           <quantityPerUnit>10 boxes x 20 bags</quantityPerUnit>
             *         </product>
             *         ...
             */

            IEnumerable<XElement> xmlProducts
                   = from r in northwindDataSet.Products
                     select
                       new XElement("product", new XAttribute("id", r.Field<int>("ProductID")),
                           new XElement("productName", r.Field<string>("ProductName")),
                           new XElement("unitPrice", r.UnitPrice),
                           new XElement("quantityPerUnit", r.Field<string>("QuantityPerUnit"))
                       );

                            XElement rootXml = new XElement("NorthwindProducts");
                            rootXml.Add(xmlProducts);

                            SaveFileDialog dlgSaveFile = new SaveFileDialog();
                            dlgSaveFile.Filter = "Pliki xml (*.xml)|*.xml";
                            if (dlgSaveFile.ShowDialog() == DialogResult.OK)
                                rootXml.Save(dlgSaveFile.FileName);
                        }
                    }
}
