using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace Exercise_LinqToObjects
{
    public class Math_exercises
    {
        [Fact]
        void FindPrimes()
        {
            const int N = 30;
            var primes = 
                Enumerable.Range(2, N - 1)
                .Where(n => !(Enumerable.Range(2, (int)Math.Sqrt(n)-1).Any(d => n % d == 0)));

            primes.Should().Equal(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);
        }

        [Fact]
        void FindPercentileForEachNumberInCollection()
        {
            int[] numbers = {20, 15, 31, 34, 35, 40, 50, 90, 99, 100};
            int[] percentiles = {10, 0, 20, 30, 40, 50, 60, 70, 80, 90};


            var result = numbers
                .ToLookup(k => k, k => numbers.Count(n => n < k))
                .Select(g => new KeyValuePair<int, double>(g.Key, 100*((double) g.First()/(double) numbers.Length)));

            var expected = numbers.Zip(percentiles, (n, p) => new KeyValuePair<int, double>(n, p));

            result.Should().Equal(expected);
        }
       
    }
}