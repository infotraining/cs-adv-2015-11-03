﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToEntities
{
    class Program
    {
        static void PrintProducts()
        {
            Console.WriteLine("\nProducts -------------------------");

            using (NorthwindEntities context = new NorthwindEntities())
            {
                IQueryable<Product> products = from p in context.Products
                                               orderby p.ProductName
                                               select p;

                foreach (Product p in products)
                    Console.WriteLine("{0, -5}|{1,-35}|{2, 10:c}|{3, -25}",
                        p.ProductID, p.ProductName, p.UnitPrice, p.QuantityPerUnit);
            }
        }

        static void PrintCategories()
        {
            using (NorthwindEntities context = new NorthwindEntities())
            {
                IQueryable<Category> categories =
                    from c in context.Categories
                    orderby c.CategoryName
                    select c;

                foreach (Category c in categories)
                {
                    Console.WriteLine("{0} - {1}", c.CategoryName, c.Description);
                }

                Console.WriteLine("\nSingle category:");
                Category cat = categories.First();

                Console.WriteLine(cat.CategoryName);
                foreach (Product p in cat.Products)
                {
                    Console.WriteLine("\t{0} - {1}", p.ProductName, p.Supplier.CompanyName);
                }
            }
        }

        static void ModifyProduct()
        {
            using (NorthwindEntities context = new NorthwindEntities())
            {
                Product product =
                    context.Products.Where(p => p.ProductID == 1).First();

                product.ProductName = "Herbata Assam";
                product.UnitPrice = 12.0M;

                context.SaveChanges();
            }
        }

        static void CreateProduct()
        {
            using (NorthwindEntities context = new NorthwindEntities())
            {
                Product p = new Product();
                p.ProductName = "Hamburger";
                p.UnitPrice = 15.0M;
                p.QuantityPerUnit = "150g na porcję";
                p.Discontinued = false;

                context.Products.Add(p);
                context.SaveChanges();
            }
        }

        static void DeleteProduct()
        {
            using (NorthwindEntities context = new NorthwindEntities())
            {
                var productsToDel = from p in context.Products
                                    where ( (p.ProductName.StartsWith("Schabowy")) || (p.ProductName.StartsWith("Hamburger"))
                                          )
                                    select p;

                foreach (Product p in productsToDel)
                    context.Products.Remove(p);

                context.SaveChanges();
            }
        }


        static void Main(string[] args)
        {
            PrintProducts();

            Console.WriteLine("\nKategorie");

            PrintCategories();

            ModifyProduct();

            CreateProduct();

            PrintProducts();

            DeleteProduct();

            PrintProducts();
        }
    }
}
