﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LinqToObjects
{
    static class EnumerableHelpers
    {
        public static void Print(this IEnumerable collection, string prefix)
        {
            Console.Write(prefix + ": [ ");
            foreach (var item in collection)
                Console.Write(item + " ");
            Console.WriteLine("]");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //OperatoryLinq();

            //ZaawansowaneZapytaniaLinq();

            Zlaczenia();

            //Grupowanie();
        }

        private static void OperatoryLinq()
        {
            int[] numbers = Enumerable.Range(1, 20).ToArray();

            List<string> names = new List<string> { "Ala", "Jonasz", "Adam", "Ewa", "Krzysztof", "Ola", "Jan", "Weronika", "Zenon", "Alicja", "Antoni", "Krystian", "Katarzyna" };

            numbers.Print("numbers");
            names.Print("names");

            Console.WriteLine("\n-------------------\n");

            #region Operator filtrujący Where

            IEnumerable<int> evenNumbers = numbers.Where(n => n % 2 == 0);
            evenNumbers.Print("evenNumbers");

            var namesA = from n in names
                         where n.StartsWith("A")
                         select n;

            namesA.Print("namesA");

            #endregion

            Console.WriteLine("\n-------------------\n");

            #region Operator projekcji Select

            var squares = numbers.Select(n => n * n);
            squares.Print("squares");

            var namesUpper = from n in names
                             select n.ToUpper();

            namesUpper.Print("namesUpper");

            var namesWithLength = names.Select(name => new { Name = name, Length = name.Length });
            namesWithLength.Print("namesWithLength");

            #endregion

            Console.WriteLine("\n-------------------\n");

            #region Operatory elementowe First, Last, Single, ElementAt

            int firstBiggerThan5 = numbers.First(n => n > 5);
            Console.WriteLine("First bigger than 5: " + firstBiggerThan5);

            string lastName = (from name in names
                               where name.StartsWith("K")
                               select name).Last();
            Console.WriteLine("Last name with K: " + lastName);

            #endregion

            Console.WriteLine("\n-------------------\n");

            #region Operatory porządkujące OrderBy, ThanBy, OrderByDescending, ThanByDescending

            var sortedNames = names.OrderBy(name => name.Length).ThenBy(name => name.ToLower());

            sortedNames.Print("sortedNames");

            var squaresDesc = from n in numbers
                              orderby n descending
                              select n * n;

            squaresDesc.Print("squaresDesc");

            var squaresReversed = squaresDesc.Reverse();

            squaresReversed.Print("squaresReversed");

            #endregion

            Console.WriteLine("\n-------------------\n");

            #region Operatory agregacji Count, Min, Max, Avg

            int numberOfEvens = numbers.Count(n => n % 2 == 0);
            Console.WriteLine("Number of evens: " + numberOfEvens);

            double avg = numbers.Average();
            Console.WriteLine("Average of numbers: " + avg);

            int maxLength = names.Max(name => name.Length);
            Console.WriteLine("Maximum length of name: " + maxLength);

            #endregion

            Console.WriteLine("\n-------------------\n");

            #region Kwantyfikatory Contains, Any, All, SequenceEquals

            bool hasNumberNine = numbers.Contains(9);
            Console.WriteLine(hasNumberNine ? "Contains 9" : "Doesn't contain 9");

            bool hasAnyLongerThan5 = names.Any(name => name.Length > 5);
            Console.WriteLine(hasAnyLongerThan5 ? "Has a name longer than 5" : "Doesn't have a name longer than 5");

            var otherNumbers = Enumerable.Range(1, 20);
            bool seqAreEqual = numbers.SequenceEqual(otherNumbers);
            Console.WriteLine(seqAreEqual ? "Sequences are equal" : "Sequences are not equal");

            #endregion

            Console.WriteLine("\n-------------------\n");

            #region Operatory zbiorów

            var otherSetOfNumbers = Enumerable.Range(15, 10);

            numbers.Print("numbers");
            otherSetOfNumbers.Print("otherSetOfNumbers");
            Console.WriteLine();

            var concatenation = numbers.Concat(otherSetOfNumbers);
            concatenation.Print("concatenation");

            var union = numbers.Union(otherSetOfNumbers);
            union.Print("union");

            var intersection = numbers.Intersect(otherSetOfNumbers);
            intersection.Print("intersection");

            var diff1 = numbers.Except(otherSetOfNumbers);
            var diff2 = otherSetOfNumbers.Except(numbers);

            diff1.Print("numbers - otherSetOfNumbers");
            diff2.Print("otherSetOfNumbers - numbers");

            #endregion

            Console.WriteLine("\n-------------------\n");

            #region Spinanie kolekcji

            List<string> lastNames = new List<string> { "Kowlska", "Kofta", "Mickiewicz", "Lipska", "Oliwa", "Nowak", "Karski", "Nowacka", "Nijaki", "Czar", "Słonimski", "Piękoś", "Nosowska" };

            var fullNames = names.Zip(lastNames, (n, ln) => string.Format("{0} {1}", n, ln));
            fullNames.Print("fullNames");

            #endregion
        }

        private static void ZaawansowaneZapytaniaLinq()
        {
            #region Slowo kluczowe let

            List<string> names = new List<string> { "Ala", "Jonasz", "Adam", "Ewa", "Krzysztof", "Ola", "Jan", "Weronika", "Zenon", "Alicja", "Antoni", "Krystian", "Katarzyna" };

            var query1 = from name in names
                         let voweless = Regex.Replace(name, "[aąeęioóuy]", "")
                         where voweless.Length > 2
                         select string.Format("({0} - {1})", name, voweless);

            query1.Print("query");

            #endregion

            Console.WriteLine("\n-------------------\n");

            #region Wielokrotne klauzule from

            var keys = Enumerable.Range(1, 3);
            var products = "Laptop Tablet Phone".Split(' ');

            var query2 = from k in keys
                         from p in products
                         select k.ToString() + "-" + p;

            query2.Print("query2");

            #endregion
        }

        private static void Zlaczenia()
        {
            var categories = new[] {
                new { ID = 1, CategoryName = "C#" },
                new { ID = 2, CategoryName = "C++" },
                new { ID = 3, CategoryName = "Python" },
                new { ID = 4, CategoryName = "Design Patterns" }
            };

            var trainings = new[] {
                new { ID = 1, CategoryID = 1, Name = "Programming in C# 5.0" },
                new { ID = 2, CategoryID = 1, Name = "Entity Framework 5.0" },
                new { ID = 3, CategoryID = 1, Name = "Programming in C# 5.0" },
                new { ID = 4, CategoryID = 2, Name = "Programming in C++" },
                new { ID = 5, CategoryID = 2, Name = "Effective STL" },
                new { ID = 6, CategoryID = 2, Name = "C++ Programming with Boost" },
                new { ID = 7, CategoryID = 3, Name = "Programming Python" }
            };

            #region CrossJoin

            var crossJoinQuery = from c in categories
                                 join t in trainings on c.ID equals t.CategoryID
                                 select new { c.CategoryName, t.Name };

            crossJoinQuery.Print("trainings with category");

            #endregion

            Console.WriteLine("\n-------------------\n");

            #region join into group

            var categoryGroups = from c in categories
                                 join t in trainings on c.ID equals t.CategoryID into trainingGroup
                                 select new { c.CategoryName, trainingGroup };

            var categoryGroupsAlt = categories.GroupJoin(trainings, c => c.ID, t => t.CategoryID,
                (c, t) => new {c.CategoryName, trainingGroup = t});

            foreach (var cg in categoryGroupsAlt)
            {
                Console.WriteLine("+ " + cg.CategoryName);
                foreach (var t in cg.trainingGroup)
                    Console.WriteLine("  - " + t.Name);
            }

            #endregion
        }

        private static void Grupowanie()
        {
            string[] files = Directory.GetFiles(Path.GetTempPath()).Take(100).ToArray();

            #region Grupowanie

            IEnumerable<IGrouping<string, string>> query1 =
                files.GroupBy(file => Path.GetExtension(file));

            foreach (IGrouping<string, string> grouping in query1)
            {
                Console.WriteLine("Extension: " + grouping.Key);

                foreach (string filename in grouping)
                    Console.WriteLine("   - " + filename);
            }

            #endregion

            #region Grupowanie z sortowaniem grup

            Console.WriteLine("\n-------------------\n");

            var query2 = from f in files
                         group f by Path.GetExtension(f) into groupedFiles
                         orderby groupedFiles.Key
                         select new { Extension = groupedFiles.Key, Count = groupedFiles.Count() };

            query2.Print("Files count by extension");

            #endregion
        }
    }
}
