﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqToXml
{
    class Car
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
    }

    class Program
    {
        static void ParseXmlDocument()
        {
            XElement root = XElement.Load(@"other_cars.xml");

            Console.WriteLine(root);

            Console.WriteLine("\nBrands:");

            foreach (var elem in root.Descendants("brand"))
                Console.WriteLine(elem.Value);

            Console.WriteLine("\nAudi models:");

            var audis = from elem in root.Descendants("brand")
                        where elem.Value == "Audi"
                        select elem.ElementsAfterSelf("model").First().Value;

            foreach (string str in audis)
                Console.WriteLine(str);
        }

        static void ModifyXmlDocument()
        {
            XElement root = XElement.Load(@"cars.xml");

            var xmlBrands = root.Descendants("brand");

            foreach (XElement elem in xmlBrands)
            {
                elem.Value = elem.Value.ToUpper();
                elem.ElementsAfterSelf("year").Remove();
            }

            root.Save(@"cars_modified.xml");
        }

        static void CreateXmlDocumentFromCollection()
        {
            Car[] myCars = { 
                new Car { Id = 1, Brand = "Toyota", Model = "Corolla", Year = 2006 },
                new Car { Id = 2, Brand = "Alfa Romeo", Model = "156", Year = 2002 },
                new Car { Id = 3, Brand = "Audi", Model = "A3", Year = 2008 },
                new Car { Id = 4, Brand = "Audi", Model = "Q7", Year = 2009 },
                new Car { Id = 4, Brand = "Audi", Model = "A7", Year = 2010 }
            };

            IEnumerable<XElement> carsXml = from car in myCars
                                            select
                                                new XElement("car", new XAttribute("id", car.Id),
                                                    new XElement("brand", car.Brand),
                                                    new XElement("model", car.Model),
                                                    new XElement("year", car.Year)
                                                    );

            XElement root = new XElement("cars");
            root.Add(carsXml);

            root.Save(@"other_cars.xml");
        }

        static void CreateXmlDocument()
        {
            XDocument doc = new XDocument(
                                new XComment("plik testowy xml"),
                                new XElement("cars",
                                    new XElement("car", new XAttribute("id", 1),
                                        new XElement("brand", "Honda"),
                                        new XElement("model", "Civic"),
                                        new XElement("year", 2000)
                                    ),
                                    new XElement("car", new XAttribute("id", 2),
                                        new XElement("brand", "Polski Fiat"),
                                        new XElement("model", "126p"),
                                        new XElement("year", 1977)
                                    ),
                                    new XElement("car", new XAttribute("id", 3),
                                        new XElement("brand", "Mazda"),
                                        new XElement("model", "6"),
                                        new XElement("year", 2008)
                                    ),
                                    new XElement("car", new XAttribute("id", 3),
                                        new XElement("brand", "Ford"),
                                        new XElement("model", "Focus"),
                                        new XElement("year", 2011)
                                   )
                                )
                            );

            doc.Save(@"cars.xml");
        }

        static void Main(string[] args)
        {
            CreateXmlDocument();
            CreateXmlDocumentFromCollection();
            ParseXmlDocument();
            ModifyXmlDocument();
        }
    }
}
