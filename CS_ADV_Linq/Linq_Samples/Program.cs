﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq_Samples
{
    class MyType: IComparable<MyType>
    {
        public string Value { get; set; }


        public int CompareTo(MyType other)
        {
            throw new NotImplementedException();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string[] words = {"one", "two", string.Empty, "four"};

            var minString = words.Min();

            Console.WriteLine(minString);

            Console.WriteLine("---------");

            MyType[] objs = {new MyType {Value = "one"}, new MyType {Value = "two"}, new MyType {Value = "1one"} };
            var defualutMin = objs.Aggregate((o1, o2) => o1.Value.CompareTo(o2.Value) < 0 ? o1 : o2);

            Console.WriteLine(defualutMin.Value);
        }
    }
}
