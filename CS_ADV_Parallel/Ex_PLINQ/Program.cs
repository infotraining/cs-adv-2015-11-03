﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Ex_PLINQ
{
    class Camera
    {
        public readonly int CameraID;

        public Camera(int cameraID)
        {
            CameraID = cameraID;
        }

        public string GetNextFrame()
        {
            var primesCount 
                = Enumerable.Range(2, 1000000)
                    .Where(n => Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(d => n % d != 0))
                    .Count();
            return "Frame from camera " + CameraID + "#" + primesCount + " - " + DateTime.Now.ToString();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Camera[] cameras = 
                Enumerable.Range(0, 16)
                .Select(i => new Camera(i)).ToArray();

            while (true)
            {
                // popraw wydajność zbierania danych z kamer
                string[] data = 
                    cameras
                       .AsParallel().WithDegreeOfParallelism(cameras.Length)
                       .Select(c => c.GetNextFrame()).ToArray();

                Console.WriteLine(string.Join("\n", data) + "\n\n");
            }

        }
    }
}
