﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFXFor
{
    class Program
    {
        static void SimpleFor()
        {
            const int SIZE = 1000;

            Guid[] keys = new Guid[SIZE];

            for (int i = 0; i < keys.Length; ++i)
                keys[i] = Guid.NewGuid();

            Parallel.For(0, keys.Length, i =>
            {
                keys[i] = Guid.NewGuid();
            });

            Parallel.For(0, keys.Length, i =>
            {
                Console.WriteLine("{0}: {1}", i, keys[i]);
            });
        }

        public static void ForWithBreak()
        {
            Parallel.For(0, 100000, (i, loopState) =>
            {
                if (i == 13)
                    loopState.Break();

                Console.WriteLine("Iteracja #{0}", i);
            });
        }

        static void Main(string[] args)
        {
            //SimpleFor();

            ForWithBreak();
        }
    }
}
