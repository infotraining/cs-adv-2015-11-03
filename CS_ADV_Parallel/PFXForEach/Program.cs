﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Concurrent;
using System.Net;
using System.Threading;

namespace PFXForEach
{
    class Program
    {
        static void Spellcheck()
        {
            // zainicjowanie slownika
            var wordLookup = new HashSet<string>(File.ReadAllLines("WordLookup.txt"), StringComparer.InvariantCultureIgnoreCase);
            Console.WriteLine("Ilosc slow w slowniku: {0}", wordLookup.Count());

            // wygenerowanie dokumentu testowego
            string[] wordList = wordLookup.ToArray();

            var localRandom = new ThreadLocal<Random>(() => new Random(Guid.NewGuid().GetHashCode()));
            string[] wordsToTest = Enumerable.Range(0, 100000)
                                        .Select(i => wordList[localRandom.Value.Next(0, wordList.Length)])
                                        .ToArray();

            // wprowadzenie błędów do dokumentu
            wordsToTest[66533] = "mizpeld";
            wordsToTest[88812] = "fenkju";
            wordsToTest[100] = "gudbaj";
            wordsToTest[1000] = "szarp";

            // sprawdzenie poprawnosci
            var misspellings = new ConcurrentBag<Tuple<long, string>>();

            Parallel.ForEach(wordsToTest, (word, loopState, i) =>
            {
                if (!wordLookup.Contains(word))
                    misspellings.Add(Tuple.Create(i, word));
            });

            foreach (var mistake in misspellings)
                Console.WriteLine(mistake.Item1 + " - index: " + mistake.Item2);
        }

        static void Main(string[] args)
        {
            Spellcheck();
        }
    }
}
