﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Net;
using System.IO;

namespace PFXInvoke
{
    class Program
    {
        static void Main(string[] args)
        {
            var actions = new Action[] 
            {
                () => { for (int i=0;i<2;i++)
                            Console.WriteLine("Task 1: " + i); },
                () => { for (int i=0;i<2;i++)
                            Console.WriteLine("Task 2: " + i); },
                () => { for (int i=0;i<2;i++)
                            Console.WriteLine("Task 3: " + i); },
            };

            Parallel.Invoke(actions);
            Console.WriteLine("Po wykonaniu zadań");


            var data = new ConcurrentBag<string>();

            Console.WriteLine("Start pobierania ---");

            Parallel.Invoke(
                () => data.Add(new WebClient().DownloadString("http://www.gazeta.pl")),
                () => data.Add(new WebClient().DownloadString("http://www.infotraining.pl")),
                () => data.Add(new WebClient().DownloadString("http://www.google.com")),
                () => data.Add(new WebClient().DownloadString("http://www.asp.net"))
            );

            Console.WriteLine("Koniec pobierania ---");

            Console.WriteLine("Zapis do plików ---");

            Parallel.ForEach(data, (d, state, i) => File.WriteAllText("data" + (i + 1).ToString() + ".txt", d));
        }
    }
}
