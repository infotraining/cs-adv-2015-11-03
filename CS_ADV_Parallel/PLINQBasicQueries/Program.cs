﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;

namespace PLINQBasicQueries
{
    static class IEnumerableHelper
    {
        public static void Print<T>(this IEnumerable<T> collection, string prefix)
        {
            Console.Write("{0}: ", prefix);
            foreach (T item in collection)
                Console.Write(item + " ");
            Console.WriteLine();
        }
    }

    class Program
    {
        static void SimplePLINQ()
        {
            var result = "abcdefghklmnoprstuwvz".AsParallel()
                .Select(c => char.ToUpper(c)).ToArray();

            result.Print("result of query");
        }

        static void Primes()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            IEnumerable<int> numbers = Enumerable.Range(2, 10000000);

            var parallelQuery =
                Enumerable.Range(2, 10000000)
                    .AsParallel()
                    .Where(n => Enumerable.Range(2, (int) Math.Sqrt(n) - 1).All(d => n%d != 0));

            int[] primes = parallelQuery.ToArray();

            stopwatch.Stop();

            primes.Take(30).Print("Primes");

            Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed.TotalSeconds);
        }

        static void BlockingIOFunctions()
        {
            var pings = from site in new[]
                        {
                            "www.google.com",
                            "www.infotraining.pl",
                            "www.gazeta.pl",
                            "www.yahoo.com",
                            "www.asp.net",
                            "www.sony.com"
                        }.AsParallel().WithDegreeOfParallelism(6)
                        let p = new Ping().Send(site)
                        select new
                        {
                            site,
                            Result = p.Status,
                            Time = p.RoundtripTime
                        };

            Console.WriteLine("Press a key to start...");
            Console.ReadKey(true);

            pings.Print("ping times");
        }

        static void Main(string[] args)
        {
            SimplePLINQ();

            Primes();

            BlockingIOFunctions();
        }
    }
}
