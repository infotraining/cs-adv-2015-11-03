﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace PLINQQueryCancellation
{
    static class IEnumerableHelper
    {
        public static void Print<T>(this IEnumerable<T> collection, string prefix)
        {
            Console.Write("{0}: ", prefix);
            foreach (T item in collection)
                Console.Write(item + " ");
            Console.WriteLine();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            int[] source = Enumerable.Range(1, 10000000).ToArray();
            CancellationTokenSource cs = new CancellationTokenSource();

            // Rozpoczęcie asynchronicznego zadania w nowym wątku. Symulacja wciśnięcia przycisku Anuluj.
            Task.Factory.StartNew(() =>
            {
                UserClicksTheCancelButton(cs);
            });

            int[] results = null;
            try
            {
                results = (from num in source.AsParallel().WithCancellation(cs.Token)
                           where num % 3 == 0
                           orderby num descending
                           select num).ToArray();

            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (AggregateException ae)
            {
                if (ae.InnerExceptions != null)
                {
                    foreach (Exception e in ae.InnerExceptions)
                        Console.WriteLine(e.Message);
                }
            }

            if (results != null)
            {
                foreach (var v in results.Take(1000))
                    Console.WriteLine(v);
            }

            Console.WriteLine();
            Console.ReadKey();

        }

        static void UserClicksTheCancelButton(CancellationTokenSource cs)
        {
            // Wciśnięcie przycisku Anuluj
            Random rand = new Random();
            Thread.Sleep(rand.Next(50, 1500));
            cs.Cancel();
        }
    }
}
