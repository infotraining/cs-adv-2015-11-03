﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;

namespace TPLBasicTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Task<string> downloadTask = Task.Factory.StartNew<string>(() =>
            {
                string content;
                using (var client = new WebClient())
                {
                    content = client.DownloadString("http://www.googl0000e.com");
                }

                Thread.Sleep(5000);
                return content;
            });

            Console.WriteLine("Pobieranie: ");

            while (!downloadTask.IsCompleted)
            {
                Console.Write("#");
                Thread.Sleep(200);
            }

            try
            {
                Console.WriteLine("\nPobrano:\n{0}", downloadTask.Result);
            }
            catch (AggregateException e)
            {
                e.Handle((ex) =>
                {
                    Console.WriteLine("Exception caught: " + ex.Message);
                    return true;
                });
            }
        }
    }
}
