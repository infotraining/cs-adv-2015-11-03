﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TPLChildTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Task parent = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Parent task started");

                Task.Factory.StartNew(() =>
                {
                    Console.WriteLine("Detached task started");
                    Thread.Sleep(10000);
                    Console.WriteLine("Detached task ended");
                });

                Task.Factory.StartNew(() =>
                {
                    Console.WriteLine("Child #1 task started");
                    Thread.Sleep(5000);
                    throw new ArgumentException("Arg error");
                    Console.WriteLine("Child #1 task ended");
                }, TaskCreationOptions.AttachedToParent);

                Task.Factory.StartNew(() =>
                {
                    Console.WriteLine("Child #2 task started");
                    Thread.Sleep(7000);
                    Console.WriteLine("Child #2 task ended");
                }, TaskCreationOptions.AttachedToParent);

                throw new ArgumentOutOfRangeException("Out of range");

                Console.WriteLine("Returnining from parent task");
            });

            try
            {
                parent.Wait();
                Console.WriteLine("Parent task ended");
            }
            catch (AggregateException ex)
            {
                ex.Flatten().Handle((e) =>
                {
                    Console.WriteLine("Handling error: " + e.Message);
                    return true;
                });
            }
            
            Console.WriteLine("END of main...");
            Console.ReadKey(true);
        }
    }
}
