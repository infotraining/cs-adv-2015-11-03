﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TPLContinueWith
{
    class Program
    {
        static List<int> FindPrimes(int start, int end, CancellationToken token)
        {
            Console.WriteLine("Finding primes started...");

            List<int> primes = new List<int>();

            for (int n = 2; n <= end; ++n)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("Finding primes has been canceled...");
                    token.ThrowIfCancellationRequested();
                }

                bool isPrime = true;

                for (int denominator = 2; denominator <= Math.Sqrt(n); ++denominator)
                {
                    if (n % denominator == 0)
                    {
                        isPrime = false;
                        break;
                    }
                }

                if (isPrime)
                    primes.Add(n);
            }

            Console.WriteLine(primes.Count() + " primes found...");
            return primes;
        }

        static void Main(string[] args)
        {
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            tokenSource.CancelAfter(TimeSpan.FromSeconds(3));
            CancellationToken token = tokenSource.Token;

            var taskFactory =
                new TaskFactory<int>(token, 
                                        TaskCreationOptions.AttachedToParent, 
                                        TaskContinuationOptions.ExecuteSynchronously,
                                        TaskScheduler.Default);

            Task parent = new Task(() =>
            {
                var childTasks = new[]
                {
                    taskFactory.StartNew(() => FindPrimes(2, 1000000, token).Count()),
                    taskFactory.StartNew(() => FindPrimes(1000000, 2000000, token).Count()),
                    //taskFactory.StartNew(() => FindPrimes(1000000, 3000000, token).Count()),
                    taskFactory.StartNew(() =>
                    {
                        Thread.Sleep(1000);
                        throw new InvalidOperationException("Error");
                    })                                                           
                };

                // if any of child tasks throw, cancel the rest of them
                foreach (var childTask in childTasks)
                {
                    childTask.ContinueWith(t => tokenSource.Cancel(), TaskContinuationOptions.OnlyOnFaulted);
                }

                // when all children are done, get the total no of primes
                taskFactory
                    .ContinueWhenAll(childTasks, 
                        completedTasks =>
                        {
                            Console.WriteLine("Continuation after all children finished their job...");
                            return completedTasks.Where(t => t.Status == TaskStatus.RanToCompletion).Sum(t => t.Result);
                        }, CancellationToken.None, TaskContinuationOptions.AttachedToParent, TaskScheduler.Default)
                    .ContinueWith(
                        t =>
                        {                           
                            Console.WriteLine("Total no of primes: {0}", t.Result);
                        },
                        TaskContinuationOptions.AttachedToParent);
            });

            // When the children are done, show any unhandled exceptions too
            parent.ContinueWith(p => {
                // I put all this text in a StringBuilder and call Console.WriteLine just once
                // because this task could execute concurrently with the task above & I don't
                // want the tasks' output interspersed
                StringBuilder sb = new StringBuilder(
                   "The following exception(s) occurred:" + Environment.NewLine);

                foreach (var e in p.Exception.Flatten().InnerExceptions)
                    sb.AppendLine("   " + e.GetType().ToString());
                Console.WriteLine(sb.ToString());
            }, TaskContinuationOptions.OnlyOnFaulted);

            parent.Start();
            try
            {
                parent.Wait();
            }
            catch (AggregateException ae)
            {
                ae.Flatten().Handle((ex =>
                {
                    Console.WriteLine("Handling exception: " + ex.Message);
                    return true;
                }));
            }
        }
    }
}
