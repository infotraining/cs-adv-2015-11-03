﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TPLTaskCancellation
{
    class Program
    {
        private static void BackgroundTask(string id, ThreadLocal<Random> random,  CancellationTokenSource tokenSource)
        {
            var token = tokenSource.Token;

            Console.WriteLine(id + " started");
            var interval = random.Value.Next(500, 10000);
            Thread.Sleep(interval);
            if (token.IsCancellationRequested)
            {
                Console.WriteLine(id + " has been cancelled...");
                token.ThrowIfCancellationRequested();
            }
            else
            {
                Console.WriteLine(id + " cancels all tasks...");
                tokenSource.Cancel();
                token.ThrowIfCancellationRequested();
            }
            Console.WriteLine(id + " is finished...");
        }

        static void Main(string[] args)
        {
            const int N = 100000;

            ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Guid.NewGuid().GetHashCode()));

            CancellationTokenSource tokenSource = new CancellationTokenSource();
            CancellationToken token = tokenSource.Token;          

            TaskFactory factory = new TaskFactory(token);

            Task[] tasks = new[]
            {
                factory.StartNew(() => BackgroundTask("Task#1", random, tokenSource)),

                factory.StartNew(() => BackgroundTask("Task#2", random, tokenSource)),
            };

            Thread.Sleep(2000);

            var findPrimes = Task.Run(() =>
            {
                Console.WriteLine("Finding primes started...");

                List<int> primes = new List<int>();

                for (int n = 2; n <= N; ++n)
                {
                    if (token.IsCancellationRequested)
                    {
                        Console.WriteLine("Finding primes has been canceled...");
                        token.ThrowIfCancellationRequested();
                    }

                    bool isPrime = true;

                    for (int denominator = 2; denominator <= Math.Sqrt(n); ++denominator)
                    {
                        if (n % denominator == 0)
                        {
                            isPrime = false;
                            break;
                        }
                    }

                    if (isPrime)
                        primes.Add(n);
                }

                Console.WriteLine(primes.Count() + " primes found...");
                return primes;
            }, token);

            try
            {
                Task.WaitAll(findPrimes, tasks[0], tasks[1]);
            }
            catch (AggregateException e)
            {
                foreach (var exeption in e.Flatten().InnerExceptions)
                {
                    Console.WriteLine("Caught: " + exeption.Message);
                }                                               
            }

            if (!findPrimes.IsCanceled)
            {
                Console.Write("Primes: ");
                foreach (var prime in findPrimes.Result.Take(100))
                    Console.Write(prime + " ");
                Console.WriteLine();
            }
        }
    }
}
