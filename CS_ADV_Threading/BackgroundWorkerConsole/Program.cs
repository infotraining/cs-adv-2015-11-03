﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading;

namespace BackgroundWorkerConsole
{
    class Program
    {
        static BackgroundWorker bw; // ---

        static void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i <= 100; i += 20)
            {
                if (bw.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                bw.ReportProgress(i);  // wskazanie postępu
                Thread.Sleep(1000);
            }

            e.Result = 123;  // ta wartość zostanie przekazana do RunWorkerCompleted
        }

        static void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Console.WriteLine("Anulowano wykonanie zadania!");
            }
            else if (e.Error != null)
            {
                Console.WriteLine("Wystąpił błąd. Wyjątek: {0}", e.Error.ToString());
            }
            else
                Console.WriteLine("Zadanie wykonane - rezultat: " + e.Result);  // zwrocenie resultatu przekazanego z RunWorkerCompleted
        }

        static void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Console.WriteLine("Wykonano {0}% zadania", e.ProgressPercentage);
        }

        static void Main(string[] args)
        {
            // utworzenie obiektu BackgroundWorker
            bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;  // bw wspiera raportowanie postępu
            bw.WorkerSupportsCancellation = true;  // bw wspiera anulowanie zadania
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);  // przy[isanie zadania
            bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);  // przypisanie delegata do raportowania postępu
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);  // przypisanie delegata do obsłużenia wyniku

            bw.RunWorkerAsync("Startuje BackgroundWorker");  // uruchomienie zadania

            Console.WriteLine("Wciśnij dowolny klawisz w ciągu 5 sek. aby anulować.");
            Console.ReadKey();

            if (bw.IsBusy)         // jeżeli jeszcze bw jest aktywny
                bw.CancelAsync();  // anulowanie zadania

            Console.ReadKey();
        }
    }
}
