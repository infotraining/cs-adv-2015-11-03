﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace BackgroundWorkerForms
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i < 100; i += 10)
            {
                int item = i;
                if (lbxItems.InvokeRequired)
                {
                    lbxItems.BeginInvoke(new MethodInvoker(
                        () =>
                        {
                            lbxItems.Items.Add("List Item #" + item.ToString());
                        }
                    ));
                }
                else
                {
                    lbxItems.Items.Add("List Item #" + i.ToString());
                }

                if (bw.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                Thread.Sleep(1000);
                bw.ReportProgress(i);
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.PerformStep();
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
                MessageBox.Show("Work cancelled");
            else
                MessageBox.Show("Work completed");

            progressBar.Value = 0;
            btnStart.Enabled = true;

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            bw.RunWorkerAsync();

            btnStart.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            bw.CancelAsync();
        }
    }
}