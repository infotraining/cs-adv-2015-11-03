using System;
using System.Threading;

namespace BankAccount
{
    internal class BankAccount
    {
        private decimal _balance;
        private object _locker = new object();
        private readonly int _id;

        public BankAccount(int id, decimal balance)
        {
            _id = id;
            Balance = balance;
        }

        public int Id
        {
            get { return _id; }
        }

        public decimal Balance
        {
            get
            {
                lock (_locker)
                {
                    return _balance;
                }
            }

            private set
            {
                lock (_locker)
                {
                    _balance = value;
                }
            }
        }

        public void Deposit(decimal amount)
        {
            lock (_locker)
            {
                Balance += amount;
            }
        }

        public void Withdraw(decimal amount)
        {
            lock (_locker)
            {
                Balance -= amount;
            }
        }

        public void Transfer(BankAccount to, decimal amount)
        {
            object locker1;
            object locker2;

            ChooseLocks(this, to, out locker1, out locker2);

            lock (locker1)
            {
                lock (locker2)
                {
                    _balance -= amount;
                    to._balance += amount;
                }
            }
        }

        private static void ChooseLocks(BankAccount account1, BankAccount account2,
            out object lk1, out object lk2)
        {
            if (account1.Id < account2.Id)
            {
                lk1 = account1._locker;
                lk2 = account2._locker;
            }
            else
            {
                lk1 = account2._locker;
                lk2 = account1._locker;
            }
        }

    }

    namespace MutexBank
    {
        internal class BankAccount
        {
            private decimal _balance;
            private Mutex _mtx = new Mutex();
            private readonly int _id;

            public BankAccount(int id, decimal balance)
            {
                _id = id;
                Balance = balance;
            }

            public int Id
            {
                get { return _id; }
            }

            public decimal Balance
            {
                get
                {
                    try
                    {
                        _mtx.WaitOne();
                        return _balance;
                    }
                    finally
                    {
                        _mtx.ReleaseMutex();
                    }
                }

                private set
                {
                    try
                    {
                        _mtx.WaitOne();
                        _balance = value;
                    }
                    finally
                    {
                        _mtx.ReleaseMutex();
                    }
                }
            }

            public void Deposit(decimal amount)
            {
                try
                {
                    _mtx.WaitOne();
                    Balance += amount;
                }
                finally
                {
                    _mtx.ReleaseMutex();
                }
            }

            public void Withdraw(decimal amount)
            {
                try
                {
                    _mtx.WaitOne();
                    Balance -= amount;
                }
                finally
                {
                    _mtx.ReleaseMutex();
                }
            }

            public void Transfer(BankAccount to, decimal amount)
            {
                Mutex[] mutexes = {this._mtx, to._mtx};

                if (Mutex.WaitAll(mutexes))
                {
                    try
                    {
                        _balance -= amount;
                        to._balance += amount;
                    }
                    finally
                    {
                        foreach (var m in mutexes)
                            m.ReleaseMutex();
                    }
                }
            }

        }
    }
}