﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BankAccount
{
    class Program
    {
        static void Main(string[] args)
        {
            var ba1 = new MutexBank.BankAccount(1, 10000.0M);
            var ba2 = new MutexBank.BankAccount(2, 10000.0M);

            Console.WriteLine("ba1.Balance = {0}; ba2.Balance = {1}", ba1.Balance, ba2.Balance);

            var transfers1 = Task.Run(() =>
            {
                for (int i = 0; i < 1000; ++i)
                    ba1.Transfer(ba2, 1.0M);
            });

            var transfers2 = Task.Run(() =>
            {
                for (int i = 0; i < 1000; ++i)
                    ba2.Transfer(ba1, 1.0M);
            });

            Thread.Sleep(200);

            try
            {
                Task.WaitAll(transfers1, transfers2);
            }
            catch (AggregateException ae)
            {
                ae.Handle(e => { Console.WriteLine(e.Message); return false; });                
            }            

            Console.WriteLine("ba1.Balance = {0}; ba2.Balance = {1}", ba1.Balance, ba2.Balance);
        }
    }
}
