﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ex_Pi
{
    class Program
    {
        const long N = 300000000;

        static void SingleThreadPi()
        {
            Console.WriteLine("SingleThreadedPi started...");
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;
            Random random = new Random(Guid.NewGuid().GetHashCode());

            for (long i = 0; i < N; ++i)
            {

                double x = random.NextDouble() * 2 - 1.0;
                double y = random.NextDouble() * 2 - 1.0;

                double length = Math.Sqrt(x * x + y * y);

                if (length < 1.0)
                    ++counter;
            }

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void ThreadUnsafePi()
        {
            Console.WriteLine("ThreadUnsafePi started...");

            int noOfThreads = Environment.ProcessorCount;
            Console.WriteLine("No of threads: {0}", noOfThreads);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;

            List<Thread> threads = new List<Thread>();
            long throwsPerThread = N / noOfThreads;

            Random random = new Random(Guid.NewGuid().GetHashCode());
            for (int i = 0; i < noOfThreads; ++i)
            {
                var thd = new Thread(() =>
                {
                    for (long j = 0; j < throwsPerThread; ++j)
                    {
                        double x = random.NextDouble() * 2 - 1.0;
                        double y = random.NextDouble() * 2 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                            ++counter;
                    }
                });

                thd.Start();
                threads.Add(thd);
            }

            foreach (var t in threads)
                t.Join();

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void PiWithLock()
        {
            Console.WriteLine("PiWithLock started...");

            int noOfThreads = Environment.ProcessorCount;
            Console.WriteLine("No of threads: {0}", noOfThreads);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;
            object locker = new object();

            List<Thread> threads = new List<Thread>();
            long throwsPerThread = N / noOfThreads;

           
            for (int i = 0; i < noOfThreads; ++i)
            {
                var thd = new Thread(() =>
                {
                    Random random = new Random(Guid.NewGuid().GetHashCode());

                    for (long j = 0; j < throwsPerThread; ++j)
                    {
                        double x = random.NextDouble() * 2 - 1.0;
                        double y = random.NextDouble() * 2 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                        {
                            lock (locker)
                            {
                                ++counter;
                            }
                        }
                    }
                });

                thd.Start();
                threads.Add(thd);
            }

            foreach (var t in threads)
                t.Join();

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void ThreadSafePiWithInterlocked()
        {
            Console.WriteLine("ThreadSafePiWithInterlocked started...");

            int noOfThreads = Environment.ProcessorCount;
            Console.WriteLine("No of threads: {0}", noOfThreads);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;

            List<Thread> threads = new List<Thread>();
            long throwsPerThread = N / noOfThreads;

            ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Guid.NewGuid().GetHashCode()));
            for (int i = 0; i < noOfThreads; ++i)
            {
                var thd = new Thread(() =>
                {
                    var randomGen = random.Value;
                    for (long j = 0; j < throwsPerThread; ++j)
                    {
                        double x = randomGen.NextDouble() * 2 - 1.0;
                        double y = randomGen.NextDouble() * 2 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                            Interlocked.Increment(ref counter);
                    }
                });

                thd.Start();
                threads.Add(thd);
            }

            foreach (var t in threads)
                t.Join();

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void ThreadSafePi()
        {
            Console.WriteLine("ThreadSafePi started...");
            int noOfThreads = Environment.ProcessorCount;
            Console.WriteLine("No of threads: {0}", noOfThreads);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;

            List<Thread> threads = new List<Thread>();
            long[] hitsByThread = new long[noOfThreads];
            long throwsPerThread = N / noOfThreads;

            ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Guid.NewGuid().GetHashCode()));
            for (int i = 0; i < noOfThreads; ++i)
            {
                int index = i;
                var thd = new Thread(() =>
                {
                    long hits = 0;
                    var randomGen = random.Value;
                    for (long j = 0; j < throwsPerThread; ++j)
                    {

                        double x = randomGen.NextDouble() * 2.0 - 1.0;
                        double y = randomGen.NextDouble() * 2.0 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                            ++hits;
                    }

                    hitsByThread[index] = hits;
                });

                thd.Start();
                threads.Add(thd);
            }

            foreach (var t in threads)
                t.Join();

            counter = hitsByThread.Sum();

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void ThreadSafePiWithFalseSharing()
        {
            Console.WriteLine("ThreadSafePiWithFalseSharing started...");
            int noOfThreads = Environment.ProcessorCount;
            Console.WriteLine("No of threads: {0}", noOfThreads);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;

            List<Thread> threads = new List<Thread>();
            long[] hitsByThread = new long[noOfThreads];
            long throwsPerThread = N / noOfThreads;

            ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Guid.NewGuid().GetHashCode()));
            for (int i = 0; i < noOfThreads; ++i)
            {
                int index = i;
                var thd = new Thread(() =>
                {
                    var randomGen = random.Value;
                    for (long j = 0; j < throwsPerThread; ++j)
                    {

                        double x = randomGen.NextDouble() * 2.0 - 1.0;
                        double y = randomGen.NextDouble() * 2.0 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                            hitsByThread[index]++;
                    }
                });

                thd.Start();
                threads.Add(thd);
            }

            foreach (var t in threads)
                t.Join();

            counter = hitsByThread.Sum();

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void TasksPi()
        {
            Console.WriteLine("Tasks started...");
            int noOfTasks = Environment.ProcessorCount;
            Console.WriteLine("No of threads: {0}", noOfTasks);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;

            long throwsPerTask = N / noOfTasks;

            ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Guid.NewGuid().GetHashCode()));

            List<Task<long>> tasks = new List<Task<long>>();

            for (int i = 0; i < noOfTasks; ++i)
            {
                tasks.Add(Task<long>.Factory.StartNew(() =>
                {
                    var randomGen = random.Value;
                    long hits = 0;
                    for (long j = 0; j < throwsPerTask; ++j)
                    {
                        double x = randomGen.NextDouble() * 2.0 - 1.0;
                        double y = randomGen.NextDouble() * 2.0 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                            ++hits;
                    }

                    return hits;
                }, TaskCreationOptions.LongRunning));
            }

            Console.WriteLine("Tasks started...");

            counter = tasks.Aggregate(0L, (i, t) => i + t.Result);

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }


        static void Main(string[] args)
        {
            SingleThreadPi();

            Console.WriteLine();

            //ThreadUnsafePi();

            //Console.WriteLine();

            PiWithLock();

            Console.WriteLine();

            ThreadSafePiWithInterlocked();

            Console.WriteLine();

            ThreadSafePi();

            Console.WriteLine();

            ThreadSafePiWithFalseSharing();

            //TasksPi();

            //Console.WriteLine();

            Console.ReadKey(true);
        }
    }
}
