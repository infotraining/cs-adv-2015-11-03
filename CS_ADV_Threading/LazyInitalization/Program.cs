﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace LazyInitalization
{
    internal class Expensive
    {
        public Expensive()
        {
            Console.WriteLine("Creating an expensive object...");
        }

        public void Process()
        {
            Console.WriteLine("Expensive object is working...");
        }
    }

    internal class Broker
    {
        #region Thread-Safe implementation of lazy initialization 

        private volatile Expensive _expensive;
        private readonly object _expensiveLock = new object();

        public Expensive Item1
        {
            get
            {
                if (_expensive == null)
                {
                    lock (_expensiveLock)
                    {
                        if (_expensive == null)
                            _expensive = new Expensive();
                    }
                }

                return _expensive;

            }
        }

        #endregion

        #region Thread-Safe implementation using Lazy<T>

        private Lazy<Expensive> _lazyExpensive = new Lazy<Expensive>(() => new Expensive(), true);

        public Expensive Item2
        {
            get { return _lazyExpensive.Value; }
        }

        #endregion
    }

    internal interface IDrawable
    {
        void Draw();
    }

    internal class Image : IDrawable
    {
        // Bitmap

        public Image(string path)
        {
            Console.WriteLine("Loading a file: " + path);
        }

        public void Draw()
        {
            Console.WriteLine("Drawing an image");
        }
    }

    internal class ImageProxy : IDrawable
    {
        private Lazy<Image> _image;

        public ImageProxy(string path)
        {
            _image = new Lazy<Image>(() => new Image(path), true);
        }

        public void Draw()
        {
            _image.Value.Draw();
        }
    }

class Client
    {
        List<IDrawable> _images = new List<IDrawable>();

        public Client(Func<string, IDrawable> imageFactory)
        {
            _images.Add(imageFactory("img1"));
            _images.Add(imageFactory("img2"));
            _images.Add(imageFactory("img3"));
        }

        public void Show()
        {
            Console.WriteLine("Show: ");

            foreach(var img in _images)
                img.Draw();           
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            Func<string, IDrawable> imageFactory = path => new ImageProxy(path);

            Client client = new Client(imageFactory);
          
            Console.ReadKey(true);

            client.Show();

            Console.WriteLine("Press a key...");
            Console.ReadKey(true);

            client.Show();

        }
    }
}
