﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace LockingThreads
{
    class Program
    {
        static List<string> list = new List<string>();

        static void AddItems()
        {
            for (int i = 0; i < 100; i++)
                lock (list)
                {
                    list.Add(string.Format("Element: {0} - dodany przez {1}", i, Thread.CurrentThread.Name));
                    Thread.Sleep(50);
                }
        }

        static void PrintItems()
        {

            try
            {
                Monitor.Enter(list);
                foreach (string str in list)
                    Console.WriteLine(str);
            }
            finally
            {
                Monitor.Exit(list);
            }
        }

        static void Main(string[] args)
        {
            Thread thd1 = new Thread(AddItems);
            thd1.Name = "X";
            Thread thd2 = new Thread(AddItems);
            thd2.Name = "Y";

            thd1.Start();
            thd2.Start();

            thd1.Join();
            thd2.Join();

            PrintItems();

            Console.ReadKey();
        }
    }
}
