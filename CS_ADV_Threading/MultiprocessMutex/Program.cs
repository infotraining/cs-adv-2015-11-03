﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MultiprocessMutex
{
    class Program
    {
        static Mutex mutex = new Mutex(false, "MultiprocessMutex.exe");

        static void Main(string[] args)
        {
            if (!mutex.WaitOne(TimeSpan.FromSeconds(15), false))
            {
                Console.WriteLine("Działa już inna aplikacja. Do widzenia!");
                return;
            }

            try
            {
                Console.WriteLine("Uruchomiono MutexDemo - wciśnij Enter...");
                Console.ReadLine();
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }
    }

}
