﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Threading;

namespace ParameterizedThread
{
    class Program
    {
        static void MainThreadInfo()
        {
            // uzyskanie i nadanie nazwy wątkowi głównemu
            Thread.CurrentThread.Name = "Main";

            // nazwa domeny aplikacji
            Console.WriteLine("Name of current app domain: {0}", Thread.GetDomain().FriendlyName);

            // id kontekstu
            Console.WriteLine("ID of current context: {0}", Thread.CurrentContext.ContextID);
        }

        static void MainThreadTask()
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Thread {0} - is working...", Thread.CurrentThread.Name);
                Thread.Sleep(1000);
            }

            Console.WriteLine("Main thread task has been finished.");
        }

        static void WorkerMethod(object o)
        {
            string text = (string)o;

            Console.WriteLine("{0} has started.", Thread.CurrentThread.Name);

            foreach(var c in text)
            {
                Console.WriteLine("Thread {0} - is working... Processing: {1}...", Thread.CurrentThread.Name, c);
                Thread.Sleep(1000);
            }

            Console.WriteLine("Thread {0} has finished.", Thread.CurrentThread.Name);
        }

        static void WorkerMethod(string text)
        {
            Console.WriteLine("{0} has started.", Thread.CurrentThread.Name);

            foreach (var c in text)
            {
                Console.WriteLine("Thread {0} - is working... Processing: {1}...", Thread.CurrentThread.Name, c);
                Thread.Sleep(1000);
            }

            Console.WriteLine("Thread {0} has finished.", Thread.CurrentThread.Name);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("***** Simple Threading *****");

            MainThreadInfo();

            // uruchomienie nowych wątków
            int threadCount = 3;


            string[] data = { "Parametrized", "thread", "demo" };
            List<Thread> threads = new List<Thread>();


            // utworzenie wątków - sposób #1
            for (int i = 0; i < data.Length; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(WorkerMethod));
                t.Name = "#A" + (i+1).ToString();
                threads.Add(t);
            }

            // uruchomienie wątków
            for (int i = 0; i < threadCount; i++)
                threads[i].Start(data[i]);

            threads.Clear();

            // utworzenie wątków - sposób #2
            for (int i = 0; i < threadCount; i++)
            {
                string text = data[i];
                Thread t = new Thread(() => { WorkerMethod(text); });
                t.Name = "#B" + (i+1).ToString();
                threads.Add(t );
            }

            foreach(var thd in threads)
                thd.Start();

             // dalsza praca wątku głównego
            MainThreadTask();
        }
    }
}
