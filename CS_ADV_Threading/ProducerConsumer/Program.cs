﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ProducerConsumer
{
    class ProducerConsumerQueue : IDisposable
    {
        EventWaitHandle wh = new AutoResetEvent(false);
        Thread worker;
        object locker = new object();

        Queue<string> tasks = new Queue<string>();

        public ProducerConsumerQueue()
        {
            worker = new Thread(Work);
            worker.Start();
        }

        public void EnqueueTask(string task)
        {
            Console.WriteLine("EnqueueTask: " + task);
            lock (locker)
                tasks.Enqueue(task);

            wh.Set();
        }

        void Work()
        {
            while (true)
            {
                string task = null;

                lock (locker)
                {
                    if (tasks.Count > 0)
                    {
                        task = tasks.Dequeue();
                        if (task == null)
                            return;
                    }
                }

                if (task != null)
                {
                    Console.WriteLine("Performing task: " + task);
                    Thread.Sleep(1000);
                }
                else
                    wh.WaitOne();
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            EnqueueTask(null);  // signal for consumer to wxit
            worker.Join();      // wait for a consumer's thread to finish
            wh.Close();         // release OS resources
        }

        #endregion
    }

    class Test
    {
        static void Main()
        {
            using (ProducerConsumerQueue q = new ProducerConsumerQueue())
            {
                q.EnqueueTask("Hello");

                for (int i = 0; i < 10; i++)
                {
                    q.EnqueueTask("Say " + i);
                    Thread.Sleep(500);
                }

                Thread.Sleep(5000);
                q.EnqueueTask("Goodbye!");
            }
        }
    }
}
