﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SimpleThread
{
    class Program
    {
        static void MainThreadInfo()
        {
            // uzyskanie i nadanie nazwy wątkowi głównemu
            Thread.CurrentThread.Name = "Main";

            // nazwa domeny aplikacji
            Console.WriteLine("Name of current app domain: {0}", Thread.GetDomain().FriendlyName);

            // id kontekstu
            Console.WriteLine("ID of current context: {0}", Thread.CurrentContext.ContextID);
        }

        static void WorkerMethod()
        {
            Random rnd = new Random();

            Console.WriteLine("{0} has started.", Thread.CurrentThread.Name);

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Thread {0} - is working...", Thread.CurrentThread.Name);
                Thread.Sleep(1000);
            }

            Console.WriteLine("Thread {0} has finished.", Thread.CurrentThread.Name);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("***** Simple Threading *****");

            MainThreadInfo();

            // uruchomienie nowych wątków
            int threadCount = 3;

            List<Thread> threads = new List<Thread>();

            // utworzenie wątków
            for (int i = 0; i < threadCount; i++)
            {
                Thread t = new Thread(new ThreadStart(WorkerMethod));
                t.Name = "#" + i.ToString();           
                threads.Add(t);
            }
            
            // uruchomienie wątków
            foreach (Thread t in threads)
                t.Start();

            // dalsza praca wątku głównego
            WorkerMethod();
        }
    }
}
