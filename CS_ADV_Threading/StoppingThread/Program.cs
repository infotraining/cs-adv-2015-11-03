﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace StoppingThread
{
    static class SomeClass
    {
        public static bool IsValid = false;
        public static bool IsComplete = false;

        public static void PrintState()
        {
            Console.WriteLine("IsValid = {0}; IsComplete = {0}", IsValid, IsComplete);
        }
    }

    class Program
    {
        static void AbortThisThread()
        {
            // ustawienie danych
            try
            {
                while (true)
                {
                    Thread.BeginCriticalRegion();
                    SomeClass.IsValid = false;
                    SomeClass.IsComplete = false;
                    Thread.EndCriticalRegion();

                    Thread.Sleep(500);

                    Thread.BeginCriticalRegion();
                    SomeClass.IsValid = true;
                    SomeClass.IsComplete = true;
                    Thread.EndCriticalRegion();

                    Thread.Sleep(500);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.ToString());

                Thread.ResetAbort();
            }

            SomeClass.PrintState();
        }

        static void Main(string[] args)
        {
            Thread newThread = new Thread(AbortThisThread);

            newThread.Start();
            
            Console.WriteLine("Press Enter to abort a thread...");
            Console.ReadLine();
            
            newThread.Abort();
        }
    }
}
