﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ThreadPoolApp
{
    class Program
    {
        static void Worker1(object o)
        {
            string txt = (string)o;

            Console.WriteLine("Starting a job in thread-pool...");

            foreach (char c in txt)
            {
                Console.Write(c + " ");
                Thread.Sleep(2000);
            }
        }

        static void Worker2(object o, bool b)
        {
            string txt = (string)o;

            foreach (char c in txt)
            {
                Console.Write(c + " ");
                Thread.Sleep(1000);
            }
        }

        static void Main(string[] args)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(Worker1), "ThreadPool");

            
            AutoResetEvent wh = new AutoResetEvent(false);
            
            ThreadPool.RegisterWaitForSingleObject(wh, Worker2, "RWFSO", Timeout.Infinite, true);

            Console.ReadKey(true);
            
            wh.Set();

            Console.ReadKey(true);
        }
    }
}
