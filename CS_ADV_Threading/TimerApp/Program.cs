﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace TimerApp
{
    class Program
    {
        static void CopyFiles(object o)
        {
            try
            {
                Console.WriteLine("CopyFiles triggered...");
                DirectoryInfo[] dirs = (DirectoryInfo[])o;
                DirectoryInfo dirFrom = dirs[0];
                DirectoryInfo dirTo = dirs[1];

                FileInfo[] filesToCopy = dirFrom.GetFiles();

                foreach (FileInfo file in filesToCopy)
                {
                    Console.WriteLine("Moving file {0} to {1}.", file.Name, dirTo.FullName);
                    file.MoveTo(dirTo.FullName + @"\" + file.Name);
                }
            }
            catch (Exception exception)
            {

                Console.WriteLine("Error: " + exception.Message);
            }
        }

        static void Main(string[] args)
        {
            // dane dotyczące katalogów
            DirectoryInfo[] dirs = { new DirectoryInfo(@"C:\Szkolenie\Timer\From"), new DirectoryInfo(@"C:\Szkolenie\Timer\To") };

            // ustawienie Timer'a
            TimerCallback timerCB = new TimerCallback(CopyFiles);

            Timer copyTimer = new Timer(timerCB, dirs, TimeSpan.Zero, TimeSpan.FromSeconds(10));

            Console.WriteLine("Press ENTER to terminate...");
            Console.ReadLine();
        }
    }
}
