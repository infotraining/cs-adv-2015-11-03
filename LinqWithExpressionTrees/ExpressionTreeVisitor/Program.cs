﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionTreeVisitor
{
    class DisplayVisitor : ExpressionVisitor
    {
        private int _level = 0;

        public override Expression Visit(Expression exp)
        {
            if (exp != null)
            {
                for (int i = 0; i < _level; ++i)
                    Console.Write("  ");
                Console.WriteLine("{0} - {1}", exp.NodeType, exp.GetType().Name);
            }
            
            _level++;
            Expression result = base.Visit(exp);
            _level--;

            return result;
        }

        public void Display(Expression exp)
        {
            Console.WriteLine("================ DisplayVisitor  ================");
            this.Visit(exp);
        }
    }

    class Program
    {
        static int Square(int x)
        {
            return x*x;
        }

        static void Main(string[] args)
        {
            Expression<Func<int, int>> expression = n => 1 + Square(n * ((n%2 == 0) ? -1 : 1));

            var displayVisitor = new DisplayVisitor();

            displayVisitor.Display(expression);

        }
    }
}
