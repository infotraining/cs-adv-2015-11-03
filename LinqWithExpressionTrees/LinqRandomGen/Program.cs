﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LinqRandomGen
{
    public class LinqToRandomQuery<T> : IOrderedQueryable<T>
    {
        private IQueryProvider _provider = null;
        private Expression _expression = null;

        public LinqToRandomQuery(IQueryProvider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("Source cannot be null");

            _provider = provider;
            _expression = Expression.Constant(this);
        }

        public LinqToRandomQuery(IQueryProvider provider, Expression expression)
        {
            if (provider == null)
                throw new ArgumentNullException("Provider cannot be null");

            if (expression == null)
                throw new ArgumentException("Expression cannot be null");

            if (!typeof(IQueryable<T>).IsAssignableFrom(expression.Type))
                throw new ArgumentOutOfRangeException("Expression");

            _provider = provider;
            _expression = expression;
        }


        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)_provider.Execute(_expression)).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_provider.Execute(_expression)).GetEnumerator();
        }

        public Expression Expression
        {
            get { return _expression; }
        }

        public Type ElementType
        {
            get { return typeof(T); }

        }

        public IQueryProvider Provider
        {
            get { return _provider; }
        }
    }


    internal class RandomGenerator<T> : IQueryProvider
    {
        private int _size;

        public RandomGenerator(int size)
        {
            if (size < 1)
                throw new ArgumentException("Size must be greater than zero...");

            _size = size;
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return new LinqToRandomQuery<T>(this, expression);
        }

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return (IQueryable<TElement>)new LinqToRandomQuery<T>(this, expression);
        }

        public object Execute(Expression expression)
        {
            return this.Translate(expression);
        }

        private IEnumerable<T> Translate(Expression expression)
        {
            QueryTranslator translator = new QueryTranslator(expression);

            bool isSorted = translator.isSorted;

            double lowerLimit = 0;
            double upperLimit = 0;

            if (typeof(T) == typeof(int)) upperLimit = int.MaxValue;
            if (typeof(T) == typeof(double)) upperLimit = 1.0;

            if (translator.isLowerLimit) lowerLimit = translator.lowerLimit;
            if (translator.isUpperLimit) upperLimit = translator.upperLimit;

            double range = upperLimit - lowerLimit;

            IEnumerable<T> randomValues = null;

            if (typeof(T) == typeof(int))
                randomValues = (IEnumerable<T>)GenerateInt(isSorted, (int)lowerLimit, (int)range);

            if (typeof(T) == typeof(double))
                randomValues = (IEnumerable<T>)GenerateDouble(isSorted, lowerLimit, range);

            return randomValues;
        }

        private List<double> GenerateDouble(bool isSorted, double lowerLimit, double range)
        {
            Random rnd = new Random();
            List<double> lst = new List<double>(_size);
            for (int i = 0; i < _size; ++i)
            {
                double number = lowerLimit + range*rnd.NextDouble();
                lst.Add(number);
            }

            if (isSorted)
                lst.Sort();

            return lst;
        }

        private List<int> GenerateInt(bool isSorted, int lowerLimit, int range)
        {
            Random rnd = new Random();
            List<int> lst = new List<int>(_size);
            for (int i = 0; i < _size; ++i)
            {
                int number = lowerLimit + rnd.Next(range);
                lst.Add(number);
            }

            if (isSorted)
                lst.Sort();

            return lst;
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return (TResult)this.Execute(expression);
        }
    }

    class QueryTranslator : ExpressionVisitor
    {
        private StringBuilder _buffer = null;
        private StringBuilder _filterBuffer = new StringBuilder();

        public Type type = null;

        public bool isLowerLimit = false;
        public bool isUpperLimit = false;
        public double lowerLimit = 0.0;
        public double upperLimit = 1.0;
        public bool isSorted = false;

        public QueryTranslator(Expression expression)
        {
            this.Visit(expression);

            Debug.WriteLine("Result for Where: " + _filterBuffer.ToString());

            string tmp = _filterBuffer.ToString();

            int pos = tmp.IndexOf('>');
            isLowerLimit = (pos >= 0);
            if (isLowerLimit)
            {
                int end = tmp.Substring(pos + 1).IndexOf(' ');
                if (end < 0)
                    end = pos + tmp.Substring(pos + 1).Length;
                string lowerLimitStr = tmp.Substring(pos + 1, end - pos);
                lowerLimit = double.Parse(lowerLimitStr);
            }

            pos = tmp.IndexOf('<');
            isUpperLimit = (pos >= 0);
            if (isUpperLimit)
            {
                int end = tmp.Substring(pos + 1).IndexOf(' ');
                if (end < 0)
                    end = pos + tmp.Substring(pos + 1).Length;
                string upperLimitStr = tmp.Substring(pos + 1, end - pos);
                upperLimit = double.Parse(upperLimitStr);
            }
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            _buffer = null;
            switch (node.Method.Name)
            {
                case "Where":
                    _buffer = _filterBuffer;
                    break;
                case "OrderBy":
                    isSorted = true;
                    break;
                case "Select":
                    break;
                default:
                    throw new NotSupportedException("Method " + node.Method.Name + " is not supported!");
            }

            if (node.Method.DeclaringType == typeof(Queryable))
            {
                this.Visit(node.Arguments[0]);
                LambdaExpression lambda = (LambdaExpression)StripQuotes(node.Arguments[1]);
                this.Visit(lambda.Body);

                return node;
            }

            throw new NotSupportedException("Method " + node.Method.Name + " is not supported!");
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            this.Visit(node.Left);

            switch (node.NodeType)
            {
                case ExpressionType.And:
                case ExpressionType.AndAlso:
                    _buffer.Append(" ");
                    break;
                case ExpressionType.LessThan:
                    _buffer.Append("<");
                    break;
                case ExpressionType.GreaterThan:
                    _buffer.Append(">");
                    break;
                default:
                    throw new NotSupportedException("Binary operator " + node.NodeType + " is not supported");
            }

            this.Visit(node.Right);

            return node;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            IQueryable q = node.Value as IQueryable;

            if (q != null)
            {
                type = q.ElementType;
            }
            else if (node.Value == null)
            {
                throw new Exception("Expression cannot be null");
            }
            else
            {
                _buffer.Append(node.Value);
            }

            return base.VisitConstant(node);
        }

        private Expression StripQuotes(Expression expression)
        {
            while (expression.NodeType == ExpressionType.Quote)
                expression = ((UnaryExpression)expression).Operand;

            return expression;
        }
    }

    internal class Program
    {
        private static void Main(string[] args)
        {
            LinqToRandomQuery<double> dataSource = new LinqToRandomQuery<double>(new RandomGenerator<double>(10));

            var rndDoubles = dataSource.Where(n => n > 0.3 && n < 0.8).OrderBy(n => n);

            Console.WriteLine("Random numbers:");

            foreach(var n in rndDoubles)
                Console.Write(n + " ");

            Console.WriteLine();

            LinqToRandomQuery<int> dataSource2 = new LinqToRandomQuery<int>(new RandomGenerator<int>(100));

            var rndInts = dataSource2.Where(n => n > 20 && n < 70).OrderBy(n => n);

            Console.WriteLine("Random numbers:");

            foreach (var n in rndInts)
                Console.Write(n + " ");
        }
    }
}
