﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DekkerAlgorithm
{
    //class Program
    //{
    //    public static int a = 0;
    //    public static int b = 0;

    //    public static void Main()
    //    {
    //        for (int i = 0; i < 10000; i++)
    //        {
    //            a = 0;
    //            b = 0;

    //            Parallel.Invoke(
    //                () => { a = 1; if (b == 0) MyConsole.Write("A"); },
    //                () => { b = 1; if (a == 0) MyConsole.Write("B"); });

    //            Console.WriteLine(System.Environment.NewLine);

    //            MyConsole.Reset();
    //        }
    //    }
    //}

    //class Program
    //{
    //    public volatile static int a = 0;
    //    public volatile static int b = 0;

    //    public static void Main()
    //    {
    //        for (int i = 0; i < 10000; i++)
    //        {
    //            a = 0;
    //            b = 0;

    //            Parallel.Invoke(
    //                () => { a = 1; if (b == 0) MyConsole.Write("A"); },
    //                () => { b = 1; if (a == 0) MyConsole.Write("B"); });

    //            Console.WriteLine(System.Environment.NewLine);

    //            MyConsole.Reset();
    //        }
    //    }
    //}

    //internal class Program
    //{
    //    public static int a = 0;
    //    public static int b = 0;

    //    public static void Main()
    //    {
    //        for (int i = 0; i < 10000; i++)
    //        {
    //            a = 0;
    //            b = 0;

    //            Parallel.Invoke(
    //                () =>
    //                {
    //                    Thread.MemoryBarrier();
    //                    a = 1;
    //                    Thread.MemoryBarrier();
    //                    if (b == 0) MyConsole.Write("A");
    //                },

    //                () =>
    //                {
    //                    Thread.MemoryBarrier();
    //                    b = 1;
    //                    Thread.MemoryBarrier();
    //                    if (a == 0) MyConsole.Write("B");
    //                }
    //                );

    //            MyConsole.Reset();

    //            Console.WriteLine(System.Environment.NewLine);
    //        }
    //    }
    //}

    internal class Program
    {
        public static volatile int a = 0;
        public static volatile int b = 0;

        public static void Main()
        {
            for (int i = 0; i < 10000; i++)
            {
                a = 0;
                b = 0;

                Parallel.Invoke(
                    () =>
                    {
                        Volatile.Write(ref a, 1);
                        if (Volatile.Read(ref b) == 0) MyConsole.Write("A");
                    },
                    () =>
                    {
                        Volatile.Write(ref b, 1);
                        if (Volatile.Read(ref a) == 0) MyConsole.Write("B");
                    });

                Console.WriteLine(System.Environment.NewLine);

                MyConsole.Reset();
            }
        }

        
        private class MyConsole
        {
            private static int counter;
            private static object locker = new object();

            public static void Write(string id)
            {
                lock (locker)
                {
                    Console.WriteLine("Wins " + id);
                    ++counter;

                    CheckValidation();
                }
            }

            private static void CheckValidation()
            {
                if (counter > 1)
                    throw new ApplicationException("Reordering occured");
            }   

            public static void Reset()
            {
                lock (locker)
                {
                    counter = 0;
                }
            }
        }
    }
}
