﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadsDemo
{
    class Program
    {
        private volatile bool isDone = false;
        private string data;

        void PrepareData()
        {
            Thread.Sleep(2000);

            data = "DANE";
            isDone = true;
            Thread.MemoryBarrier();

            Console.WriteLine("End of thread {0} is ready", data);
        }

        static void Main(string[] args)
        {
            Program program = new Program();

            Thread thd1 = new Thread(() => program.PrepareData());
            
            thd1.Start();
            thd1.Name = "THD#1";
            thd1.IsBackground = true;

            while (!program.isDone)
            {
            }

            Console.WriteLine("Processing {0}", program.data);

            thd1.Join();
        }
    }
}

