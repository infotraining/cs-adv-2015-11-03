﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PLINQ_Demo
{
    static class DictExt
    {
        public static Dictionary<int, int> Merge(this Dictionary<int, int> first, Dictionary<int, int> second)
        {
            foreach (var k in second.Keys)
            {
                if (first.ContainsKey(k))
                    first[k] += second[k];
                else
                {
                    first.Add(k, second[k]);
                }
            }

            return first;
        }

        public static Dictionary<int, int> Update(this Dictionary<int, int> dict, int item)
        {
            if (dict.ContainsKey(item))
                dict[item]++;
            else
                dict.Add(item, 1);
                    
            return dict;
        } 
    }

    class Program
    {
        static void Main(string[] args)
        {
            var results = Enumerable.Range(1, 20).AsParallel().Select(x => Math.Pow(x, 3));

            Console.WriteLine("Cubes: ");
            foreach (var r in results)
                Console.Write(r + " ");
            Console.WriteLine();


            var sw = new Stopwatch();
            sw.Start();

            const int N = 20000;

            Console.WriteLine("#SyncPrimes");

            var primes =
                Enumerable
                    .Range(2, N)
                    .Where(n => Enumerable.Range(2, n - 2).All(d => n % d != 0));

            var result = primes.ToArray();

            sw.Stop();

            Console.WriteLine("Primes: ");
            foreach (var prime in result.Take(20))
                Console.Write(prime + " ");
            Console.WriteLine("\nTime: {0}", sw.ElapsedMilliseconds);

            sw.Reset();
            sw.Start();

            Console.WriteLine("#PLINQPrimes");

            CancellationTokenSource tokenSource = new CancellationTokenSource();
            CancellationToken token = tokenSource.Token;

            tokenSource.CancelAfter(2000);
            //Task.Run(() =>
            //{
            //    Thread.Sleep(2000);
            //    tokenSource.Cancel();
            //});


            primes =
                    Enumerable
                        .Range(2, N).AsParallel().AsOrdered().WithCancellation(token)
                        .Where(n => Enumerable.Range(2, n - 2).All(d => n % d != 0));


            try
            {
                result = primes.ToArray();

                sw.Stop();

                Console.WriteLine("Primes: ");
                foreach (var prime in result.Take(20))
                    Console.Write(prime + " ");
                Console.WriteLine("\nTime: {0}", sw.ElapsedMilliseconds);
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine("Anulowana zapytanie LINQ");
            }

            var hosts = new List<string>
            {
                "googlddde.com",
                "yahoo.com",
                "abb.com",
                "nbp.pl",
                "wpddd.pl",
                "google.com",
                "yahoo.com",
                "abb.com",
                "nbp.pl",
                "wp.pl"
            };

            var pages = hosts.AsParallel().WithDegreeOfParallelism(10).Select(h =>
            {
                Console.WriteLine("Starting {0}", h);
                return new {
                    Host = h,
                    Content = new WebClient().DownloadString("http://" + h)
                };
            });

            try
            {
                foreach (var p in pages)
                {
                    Console.WriteLine("Host: {0}; Content: ", p.Host);
                    Console.WriteLine(p.Content.Take(60).ToArray());
                }
            }
            catch (AggregateException ae)
            {
                //ae.Handle((exception) =>
                //{
                //    Console.WriteLine("Error: " + exception.Message);

                //    return true;
                //});

                foreach (var exception in ae.Flatten().InnerExceptions)
                {
                    Console.WriteLine("Error: " + exception.Message);
                }
            }

            // Aggregate

            Random rnd = new Random();

            var data = new List<int>();
            for(int i = 0; i < 10000; ++i)
                data.Add(rnd.Next(1000));

            var dict = data
                .AsParallel()
                .Aggregate(() => new Dictionary<int, int>(), // fabryka wartosci poczatkowych
                           (subtotal, item) => subtotal.Update(item), 
                           (total, subtotal) => total.Merge(subtotal), total => total);

            Console.WriteLine("No of keys: {0}", dict.Keys.Count());
            foreach (var kv in dict)
            {
                Console.WriteLine("{0} : {1}", kv.Key, kv.Value);
            }
        }
    }
}
