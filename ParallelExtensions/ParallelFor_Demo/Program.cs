﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelFor_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Parallel.For(1, 20, (i, loopState) =>
            {
                Console.WriteLine("Processing: {0}", i);
                if (i == 13)
                    loopState.Stop();
            });
            }
    }
}
